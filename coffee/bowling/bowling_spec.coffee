Bowling = require './bowling'

# [1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6] => 133

describe 'Bowling All game gutter', ->
  beforeEach ->
    @game = new Bowling()
    for i in [1..20]
      @game.roll(0)
  it 'score is 0', ->
    expect(@game.score()).toEqual(0)

describe 'Bowling All game get 1pins', ->
  beforeEach ->
    @game = new Bowling()
    for i in [1..20]
      @game.roll(1)
  it 'score is 20', ->
    expect(@game.score()).toEqual(20)

describe 'Bowling in case of strike', ->
  beforeEach ->
    @game = new Bowling()
    @game.roll(10)
    @game.roll(3)
    @game.roll(4)
    for i in [1..18]
      @game.roll(0)
  it 'score is 24', ->
    expect(@game.score()).toEqual(24)

describe 'Bowling in case of perfect game', ->
  beforeEach ->
    @game = new Bowling()
    for i in [1..12]
      @game.roll(10)
  it 'score is 300', ->
    expect(@game.score()).toEqual(300)

describe 'Bowling in case of spare', ->
  beforeEach ->
    @game = new Bowling()
    @game.roll(5)
    @game.roll(5)
    @game.roll(4)
    @game.roll(3)
    for i in [1..16]
      @game.roll(0)
  it 'score is 21', ->
    expect(@game.score()).toEqual(21)

# [1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6] => 133
describe 'Bowling in case of test game', ->
  beforeEach ->
    @game = new Bowling()
    for pins in [1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6]
      @game.roll(pins)
  it 'score is 133', ->
    expect(@game.score()).toEqual(133)
