class Bowling
  constructor: ->
    @rolls = []

  roll:(pins) ->
    @rolls.push pins

  score: ->
    point = 0
    roll_idx = 0
    for i in [1..10]
      if this.strike roll_idx
        point += this.strike_bonus(roll_idx)
        roll_idx += 1
      else if this.spare roll_idx
        point += this.spare_bonus(roll_idx)
        roll_idx += 2
      else
        point += this.score_of_frame roll_idx
        roll_idx += 2
    point

  strike:(roll_idx) ->
    @rolls[roll_idx] == 10

  spare:(roll_idx) ->
    @rolls[roll_idx] + @rolls[roll_idx + 1] == 10

  score_of_frame:(roll_idx) ->
    @rolls[roll_idx] + @rolls[roll_idx + 1]

  strike_bonus:(roll_idx) ->
    10 + @rolls[roll_idx + 1] + @rolls[roll_idx + 2]

  spare_bonus:(roll_idx) ->
    10 + @rolls[roll_idx + 2]

module.exports = Bowling
