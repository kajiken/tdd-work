class MessageFilter
  def initialize(*words)
    @ng_words = words
  end

  def detect?(text)
    @ng_words.any? { |w| text.include?(w) }
  end

  attr_reader :ng_words
end
